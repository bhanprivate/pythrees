#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 21:17:18 2016

@author: bhan
"""

import math

class blockElement:
    'Basic class for a block element in the game'

    def __init__ (self, value = 0):#, position = -1):
        self.__value = value
#        self.__position = position
    
    def __str__ (self):
#        return ("Value " + str(self.__value) + " @ position " + str(self.__position))
        return(str(self.__value))

    def getValue(self):
        return self.__value
        
    def setValue(self, value):
        self.__value = value
        
#    def setPosition (self, position):
#        self.__position = position
        
    def elementScore(self):
        if self.getValue() <= 3:
            return self.getValue()
        else:
            return 3**(int(math.log2(self.getValue()/3))+1)