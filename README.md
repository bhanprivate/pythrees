# README #


### INTRODUCTION ###

This project simply simulates the mobile phone game "[THREES!](http://asherv.com/threes/)" (a js simulator is available [here](http://threesjs.com))

### SETUP ###

The game can be directly executed by python, without installation. Python 3.0 is required.

* Clone the repository to your local folder:
```
#!
git clone https://hoknethan@bitbucket.org/bhanprivate/pythrees.git
```

* Install Python 3:
e.g. with apt-get:
```
#!
sudo apt-get install python3
```

* Run the game under terminal:
```
#!
python3 PyThrees.py
```

* You can also add the -b option to set a bonus level, so that at the beginning of the game you will have a bonus block with large value, e.g.
```
#!
python3 PyThrees.py -b 5
```