#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 21:58:27 2016

@author: bhan
"""
import sys, getopt
from gameTable import gameTable
from gameTableGUI import gameTableGUI

def main():
    bonusLevel=6
    try:
        opts, args = getopt.getopt(sys.argv[1:],'hb:',['help','bonuslevel='])
    except getopt.GetoptError:
        print ('PyThrees.py -b <bonuslevel>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            print ('PyThrees.py -b <bonuslevel>')
            sys.exit()
        elif opt in ('-b', '--bonuslevel'):
            bonusLevel = int(float(arg))
        else:
            assert False, 'unhandled option'
    gameTableGUI(bonusLevel)
#    gameTable(bonusLevel)

if __name__ == "__main__":
   main()