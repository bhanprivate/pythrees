#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 21:17:18 2016

@author: bhan
"""
#import sys
#import select
from blockElement import blockElement
from random import randint

def generateBlockValue(maxLevel):
    lvl = randint(-2,maxLevel)
    if lvl == -2:
        return 1
    elif lvl == -1:
        return 2
    else:
        return 2 ** lvl * 3
        
def pairMatch(num1, num2):
    if (num1>2) & (num2>2) & (num1 == num2):
        return True
    elif num1 + num2 == 3:
        return True
    else:
        return False


class gameTable:
    'Class for the game table'
    
    def __init__(self, bonusLevel = 0, currentLevel = -1):
#        self.__exitGame = False
        self.__elements = [blockElement() for i in range(16)]
        self.__bonusLevel = bonusLevel
        self.__currentLevel = currentLevel
        self.__entrances = []
        self.__updateBonusBlockValue()
#        self.__updateMaxBlockValue()
        self.__setInitElements()
        self.__generateNextBlock()
#        self.display()
        self.__checkMoves()
        self.display()
#        while (self.isGameOver() == False):# & (self.__exitGame == False):
#            if self.getNextMove() == 1:
#                print ('Byebye!')
#                return
#            self.__placeNewBlock()
#            self.updateCurrentLevel()
#            self.__generateNextBlock()
#            self.__checkMoves()
#            self.display()
        self.showScore()
#        print ('Game over!')
    
    def showScore(self):
        score = 0
        for e in self.__elements:
            score = score + e.elementScore()
#        print ('Game over! You got ' + str(int(score))+' points!')
            print(score)
            
#    def __updateMaxBlockValue(self):
#        if self.__currentLevel == 0:
#            self.__maxBlockValue = 3
#        else:
#            self.__maxBlockValue = 2 ** self.__currentLevel * 3
            
    def __updateBonusBlockValue(self):
        if self.__bonusLevel == 0:
            self.__bonusBlockValue = 3
        else:
            self.__bonusBlockValue = 2 ** self.__bonusLevel * 3
            
    def __setInitElements(self):
        self.__elements[0].setValue(self.__bonusBlockValue)
        for i in range (0,8):
            pos = randint(1,15)
            while self.__elements[pos].getValue() != 0:
                pos = randint(1,15)
            lvl = randint(-2,self.__currentLevel)
            if lvl == -2:
                value = 1
            elif lvl == -1:
                value = 2
            else:
                value = 2 ** lvl * 3
            self.__elements[pos].setValue(value)
            
    def __generateNextBlock(self):
        self.__nextBlock = blockElement(generateBlockValue(self.__currentLevel) )

    def display(self):
#        os.system('cls' if os.name == 'nt' else 'clear')
#        print ("Bonus level = " + str(self.__bonusLevel) + ", current level = " + str(self.__currentLevel))# + ", maximal block value = " + str(self.__maxBlockValue))
#        for i in range (0,16):
#            self.__elements[i].setPosition(i)
#            print (str(self.__elements[i]))
        for i in range (0,4):
            self.__currentRow = "  "
            for j in range (0,4):
                self.__currentRow = self.__currentRow + str(self.__elements[i*4+j])+"  "
            print (self.__currentRow)
        print ('Coming: '+str(self.__nextBlock.getValue()))
#        print (' Possible move(s) in row(s) '+str(self.__movableRows) + ' and column(s) '+str(self.__movableColumns))

    def __checkMoves(self):
        self.__movableRows = self.__checkDirectionalMove('h')
        self.__movableColumns = self.__checkDirectionalMove('v')
#        print ('Possible move(s) in row(s) '+str(self.__movableRows) + ' and column(s) '+str(self.__movableColumns))

    def __checkDirectionalMove(self, direction):
        movable = [];
        if direction == ('h'):
            for i in range (0,4):
                if self.__checkRowMove(i):
#                    print ('Possible horizontal move in row '+str(i))
                    movable.append(i)
        else:
            for i in range (0,4):
                if self.__checkColumnMove(i):
#                    print ('Possible vertical move in column '+str(i))
                    movable.append(i)
        return movable

    
    def __checkRowMove(self, row):
        for i in range (0,3):
            if self.__elements[row*4+i].getValue() == 0:
                return True
            if pairMatch(self.__elements[row*4+i].getValue(), self.__elements[row*4+i+1].getValue()):
                return True
        if self.__elements[row*4+3].getValue() == 0:
            return True
        else:
            return False
        
    def __checkColumnMove(self, column):
        for i in range (0,3):
            if self.__elements[column+i*4].getValue() == 0:
                return True
            if pairMatch(self.__elements[column+i*4].getValue(), self.__elements[column+(i+1)*4].getValue()):
                return True
        if self.__elements[column+3*4].getValue() == 0:
            return True
        else:
            return False
    
    def isGameOver(self):
        return (len(self.__movableRows)+len(self.__movableColumns) == 0)
        
    def getNextMove(self):
        successfulMove = False
        while (successfulMove == False):
            direction = input('Up (W)? Down (S)? Left (A)? Right (D)? Exit (E)?')
            if (direction is 'W') | (direction is 'w'):
                successfulMove = self.__moveUp()
            elif (direction is 'S') | (direction is 's'):
                successfulMove = self.__moveDown()
            elif (direction is 'A') | (direction is 'a'):
                successfulMove = self.__moveLeft()
            elif (direction is 'D') | (direction is 'd'):
                successfulMove = self.__moveRight()
            elif (direction is 'E') | (direction is 'e'):
#                successfulMove = True
#                self.__exitGame = True
                return (1)
            else:
                print('Invalid input')
        return(0)
    
    def __moveUp(self):
        if len(self.__movableColumns) <= 0:
            return False
        else:
            for i in self.__movableColumns:
                self.__moveOneColumnUp(i)
            return True
            
    def __moveDown(self):
        if len(self.__movableColumns) <= 0:
            return False
        else:
            for i in self.__movableColumns:
                self.__moveOneColumnDown(i)
            return True
            
    def __moveLeft(self):
        if len(self.__movableRows) <= 0:
            return False
        else:
            for i in self.__movableRows:
                self.__moveOneRowLeft(i)
            return True

    def __moveRight(self):
        if len(self.__movableRows) <= 0:
            return False
        else:
            for i in self.__movableRows:
                self.__moveOneRowRight(i)
            return True
            
    def __moveOneColumnUp(self, column):
        movingMode = 0 # adding two blocks
        for row in range (0,3):
            if self.__elements[row*4+column].getValue == 0:
                movingMode = 1 # pure shifting
                firstBlank = row
        if movingMode == 0:
            for row in range (0,3):
                if pairMatch(self.__elements[row*4+column].getValue(), self.__elements[(row+1)*4+column].getValue()) | (self.__elements[row*4+column].getValue()  == 0):
                    self.__elements[row*4+column].setValue( self.__elements[row*4+column].getValue() + self.__elements[(row+1)*4+column].getValue() )
                    self.__elements[(row+1)*4+column].setValue(0)
            self.__entrances.append(12+column)
        else:
            for row in range (firstBlank,3):
                self.__elements[row*4+column].setValue( self.__elements[(row+1)*4+column].getValue() )
        self.__entrances.append(12+column)
        self.__elements[12+column].setValue(0)
        
    def __moveOneColumnDown(self, column):
        movingMode = 0 # adding two blocks
        for i in range (0,3):
            row = 3-i
            if self.__elements[row*4+column].getValue == 0:
                movingMode = 1 # pure shifting
                firstBlank = row
        if movingMode == 0:
            for i in range (0,3):
                row = 3-i
                if pairMatch(self.__elements[row*4+column].getValue(), self.__elements[(row-1)*4+column].getValue()) | (self.__elements[row*4+column].getValue()  == 0):
                    self.__elements[row*4+column].setValue( self.__elements[row*4+column].getValue() + self.__elements[(row-1)*4+column].getValue() )
                    self.__elements[(row-1)*4+column].setValue(0)
            self.__entrances.append(column)
        else:
            for i in range (3-firstBlank,3):
                row = 3-i
                self.__elements[row*4+column].setValue( self.__elements[(row-1)*4+column].getValue() )
        self.__entrances.append(column)
        self.__elements[column].setValue(0)
        
    def __moveOneRowLeft(self, row):
        movingMode = 0 # adding two blocks
        for column in range (0,3):
            if self.__elements[row*4+column].getValue == 0:
                movingMode = 1 # pure shifting
                firstBlank = column
        if movingMode == 0:
            for column in range (0,3):
                if pairMatch(self.__elements[row*4+column].getValue(), self.__elements[row*4+column+1].getValue()) | (self.__elements[row*4+column].getValue()  == 0):
                    self.__elements[row*4+column].setValue( self.__elements[row*4+column].getValue() + self.__elements[row*4+column+1].getValue() )
                    self.__elements[row*4+column+1].setValue(0)
            self.__entrances.append(row*4+3)
        else:
            for column in range (firstBlank,3):
                self.__elements[row*4+column].setValue( self.__elements[row*4+column+1].getValue() )
        self.__entrances.append(row*4+3)
        self.__elements[row*4+3].setValue(0)
        
        
    def __moveOneRowRight(self, row):
        movingMode = 0 # adding two blocks
        for i in range (0,3):
            column = 3-i
            if self.__elements[row*4+column].getValue == 0:
                movingMode = 1 # pure shifting
                firstBlank = column
        if movingMode == 0:
            for i in range (0,3):
                column= 3-i
                if pairMatch(self.__elements[row*4+column].getValue(), self.__elements[row*4+column-1].getValue()) | (self.__elements[row*4+column].getValue()  == 0):
                    self.__elements[row*4+column].setValue( self.__elements[row*4+column].getValue() + self.__elements[row*4+column-1].getValue() )
                    self.__elements[row*4+column-1].setValue(0)
            self.__entrances.append(row*4)
        else:
            for i in range (3-firstBlank,3):
                column = 3-i
                self.__elements[row*4+column].setValue( self.__elements[row*4+column-1].getValue() )
        self.__entrances.append(row*4)
        self.__elements[row*4].setValue(0)
        
    def __placeNewBlock(self):
        entrancePick = randint(0,len(self.__entrances)-1)
        entrance = self.__entrances[entrancePick]
        self.__entrances = []
        self.__elements[entrance].setValue(self.__nextBlock.getValue())
    
    def updateCurrentLevel(self):
        existingValues = []
        for i in range (0,16):
            existingValues.append(self.__elements[i].getValue())
        self.__currentLevel = int(max(existingValues)/48)